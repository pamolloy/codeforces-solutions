// Extreme subtraction
// Target: 2s, 256MB
// https://codeforces.com/problemset/problem/1442/A

#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

// TODO(PM): This solution prints NO for the following test cases, while the
// expected output is YES. That is because I didn't consider the possibility of
// decrementing more than one element from both sides.
//      11 11 12 14 11 12
//      14 18 19 11
//      11 6 7 10 9 9 8
//      11 13 12 12 11

int
main(int argc, char** argv)
{
  uint32_t tc_num; // Number of test cases
  scanf("%i", &tc_num);
  for (; tc_num > 0; tc_num--) {
    bool success = true;
    ;
    bool decreasing = true;
    uint32_t tc_len; // Length of a test case
    uint32_t prev_val;
    scanf("%i", &tc_len);
    scanf("%i", &prev_val);

    fprintf(stderr, "tc_len=%i\n", tc_len);
    for (tc_len--; tc_len > 0; tc_len--) {
      uint32_t val;
      scanf("%i", &val);
      if (success) {
        fprintf(stderr, "prev_val=%i, val=%i\n", prev_val, val);
        if (decreasing && (val > prev_val) && (tc_len > 1)) {
          uint32_t next_val;
          decreasing = false;
          scanf("%i", &next_val);
          tc_len--;
          if ((prev_val + next_val) < val) {
            fprintf(stderr,
                    "prev_val=%i, val=%i, next_val=%i\n",
                    prev_val,
                    val,
                    next_val);
            printf("NO\n");
            success = false;
          }
          val = next_val;
        } else if (!decreasing && (val < prev_val)) {
          fprintf(stderr, "Decreasing when should be increasing\n");
          printf("NO\n");
          success = false;
        }
      }
      prev_val = val;
    }
    if (success)
      printf("YES\n");
  }

  return 0;
}
