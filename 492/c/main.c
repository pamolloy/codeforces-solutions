// Vanya and Exames
// Target: 1s, 256MB
// https://codeforces.com/contest/492/problem/C

#include <math.h>
#include <stdint.h>
#include <stdio.h>

uint32_t nexams; // Number of exams
uint32_t mgrade; // Maximum possible grade
uint32_t ravg;   // Required average

int32_t rpoints; // Points required to reach required average
// Each element is a count of the number of times a particular value was
// scanned for nessay
#define MESSAYS 1000001
uint32_t nnessays[MESSAYS];

int
main()
{
  scanf("%i", &nexams); // 1 <= n <= 10^5
  scanf("%i", &mgrade); // 1 <= r <= 10^9
  scanf("%i", &ravg);   // 1 <= avg <= min(r, 10^6)

  fprintf(stderr, "Number of exams: %i\n", nexams);
  fprintf(stderr, "Maximum grade: %i\n", mgrade);
  fprintf(stderr, "Required average: %i\n", ravg);

  uint32_t grade = 0;
  // Number of essays required to increase the grade by one point
  uint32_t nessays = 0;
  while (nexams) {
    scanf("%i", &grade);   // 1 <= a <= r
    scanf("%i", &nessays); // 1 <= b <= 10^6

    rpoints += (ravg - grade);
    nnessays[nessays] += mgrade - grade;

    nexams--;
    fprintf(stderr, "Grade: %i, Essays: %i\n", grade, nessays);
  }

  fprintf(stderr, "Required points: %i\n", rpoints);
  if (rpoints < 1) {
    printf("0\n");
    return 0;
  }

  uint64_t ressays = 0; // Required essays
  for (uint32_t i = 0; i < MESSAYS; i++) {
    while (nnessays[i] > 0 && rpoints > 0) {
      ressays += i;
      nnessays[i]--;
      rpoints--;
    }

    if (rpoints < 1) {
      printf("%lu\n", ressays);
      return 0;
    }
  }

  // TODO(PM): Return error code if rpoints > 0

  return 0;
}
