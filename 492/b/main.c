// Vanya and Lanterns
// Target: 1s, 256MB
// https://codeforces.com/problemset/problem/492/B

#include <math.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

struct lantern
{
  struct lantern* prev;
  struct lantern* next;
  uint32_t pos;
};

static struct lantern* head;
static struct lantern* tail;

double
max_radius(struct lantern* l)
{
  double max = 0;
  while (l->next) {
    uint32_t diff = l->next->pos - l->pos;
    if (diff > max)
      max = diff;
    l = l->next;
  }
  return max / 2;
}

void
print_street(struct lantern* l)
{
  do {
    fprintf(stderr, "%i", l->pos);
    l = l->next;
    if (l)
      fprintf(stderr, ", ");
  } while (l);
  fprintf(stderr, "\n");
}

void
place_lantern(struct lantern* lant, struct lantern* new)
{
  while (new->pos > lant->pos) {
    fprintf(stderr, "last=%i, new=%i\n", lant->pos, new->pos);
    if (!lant->next) { // Reached end of list
      new->prev = lant;
      new->next = lant->next; // NULL
      lant->next = new;
      tail = new;
      return;
    }

    lant = lant->next;
  }

  new->prev = lant->prev;
  new->next = lant;

  if (lant->prev) {
    lant->prev->next = new;
  } else {
    head = new;
  }
  lant->prev = new;
}

int
main(int argc, char** argv)
{
  uint32_t nlan; // Number of lanterns
  uint32_t len;  // Length of street

  scanf("%i", &nlan); // TODO(PM): Parse uint16_t
  scanf("%i", &len);
  fprintf(stderr, "nlan=%i, len=%i\n", nlan, len);

  struct lantern first;
  scanf("%i", &first.pos);
  nlan--;
  first.next = NULL;
  first.prev = NULL;
  head = &first;
  tail = &first;

  while (nlan) {
    struct lantern* l = (struct lantern*)malloc(sizeof(struct lantern));
    scanf("%i", &l->pos);
    fprintf(stderr, "pos=%i\n", l->pos);
    nlan--;

    place_lantern(head, l);
    print_street(head);
  }

  fprintf(stderr, "head=%i, tail=%i\n", head->pos, tail->pos);
  printf("%f\n", fmax(max_radius(head), fmax(head->pos, (len - tail->pos))));

  // TODO(PM): Free the lanterns
  exit(0);
}
